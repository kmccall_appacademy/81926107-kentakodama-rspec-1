
def echo(string)
    string
end

def shout(string)
    words = string.split(" ")
    words.map! {|word| word.upcase }
    words.join(" ")
end

def repeat(string, num=2)
    phrase = ""
    num.times {phrase += "#{string} "}
    phrase.chop
end

def start_of_word(string, num)
    string.slice(0, num)
end

def first_word(string)
    words = string.split(" ")
    words.first
end

def titleize(string)
    words = string.split(" ")
    titleized_words = []
    little_words = ["the", "and", "or", "but", "a", "an", "over"]
    words.each_with_index do|word, index|
        if index == 0
            titleized_words << word.capitalize
        elsif little_words.include? word
            titleized_words << word
        else
            titleized_words << word.capitalize
        end
    end

    titleized_words.join(" ")
end


puts titleize("david copperfield")
