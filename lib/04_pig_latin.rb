def translate(string)
    # split into array of words
    words = string.split(" ")
     # create vowel array
    vowels = ("aeiou").split("")
    # iterate over each
    punctuation = (".,!?").split("")

    words.map! do |word|
        capitalized = false
        if word == word.capitalize
            capitalized = true
            word.downcase!
        end

        if punctuation.include? word[-1]
            non_letter = word[-1]
            word.chop!
        end

        if word.include?("qu")
            count = (word.index("qu")+2)
            translated = word[count..-1] + word.slice(0, count) + "ay" + non_letter.to_s
            if capitalized
                translated.capitalize
            else
                translated
            end
        else
            count = 0
            i = 0
            while i < word.length
                if !vowels.index word[i]
                    count += 1
                else
                    break
                end
                i += 1
            end

            translated = word[count..-1] + word.slice(0, count) + "ay" + non_letter.to_s
            if capitalized
                translated.capitalize
            else
                translated
            end
        end
    end

    words.join(" ")
    

end
