def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(numbers)
  return 0 if numbers.empty?
  numbers.reduce(:+)
end

def multiply(*args)
  args.reduce(:*)
end

def power(a, b)
  a ** b
end

def factorial(num)
  return 0 if num == 0
  (1..num).to_a.reduce(:*)
end
