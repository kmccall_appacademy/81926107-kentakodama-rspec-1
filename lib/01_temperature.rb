def ftoc(f)
  (f - 32.0) * 5 / 9
end

def ctof(c)
  (9.0/5.0) * c + 32
end
